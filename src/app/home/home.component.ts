import { Component, OnInit } from '@angular/core';
import { Person } from '../entities';
import { PersonService } from '../person.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  selected?:Person;
  persons: Person[] = [];
  constructor(private service: PersonService) { }

  ngOnInit(): void {
    this.service.getAll().subscribe(data => this.persons = data);
  }

  fetchPerson(id:number) {
    this.service.getById(id).subscribe(data => this.selected = data);
  }  

}
