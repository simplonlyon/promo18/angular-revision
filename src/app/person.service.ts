import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Person } from './entities';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(private http:HttpClient) { }

  getAll():Observable<Person[]> {
    return this.http.get<Person[]>('http://localhost:8080/api/person');
  }

  getById(id:number):Observable<Person> {
    return this.http.get<Person>('http://localhost:8080/api/person/'+id);
  }

}
