import { Component, Input, OnInit } from '@angular/core';
import { Person } from '../entities';

@Component({
  selector: 'app-person-modal',
  templateUrl: './person-modal.component.html',
  styleUrls: ['./person-modal.component.css']
})
export class PersonModalComponent implements OnInit {
  @Input()
  person?:Person;
  

  constructor() { }

  ngOnInit(): void {
  }

}
