

export interface Skill {
    id:number;
    label:string;
}

export interface Person {
    id:number;
    name:string;
    firstName:string;
    birthdate:string;
    skills: Skill[];
}