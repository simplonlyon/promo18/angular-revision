# AngularRevision

## Exercices
1. Créer un service person avec le ng g s
2. Rajouter dans les imports du AppModule le HttpClientModule
3. Injecter le HttpClient dans le constructeur du PersonService 
4. Créer une méthode getAll() qui va faire une requête http get vers http://localhost:8080/api/person et return un tableau de Person
5. Dans le HomeComponent, injecter le PersonService dans le constructeur, créer une propriétés persons de type tableau de Person et dans le ngOnInit faire un subscribe sur le getAll comme on a fait hier pour assigner les persons à la propriété
6. Faire le template avec un petit ngFor sur les persons pour les affichers toutes 

### Sélection d'une personne
1. Créer un component PersonModal qui aura une propriété person:Person avec un @Input  dessus
2. Dans son template, afficher les informations de la personne ainsi que ses skills avec un ngFor sur ces derniers
3. Rajouter une méthode getOne(id:number) dans le PersonService qui va faire une requête sur la route avec id
4. Dans le HomeComponent, rajouter une propriété selected?:Person ainsi qu'une méthode fetchPerson(id:number) qui va faire un subscribe sur le getOne et assigner le data à selected
5. Dans le template, faire qu'au click sur une personne on déclenche le fetchPerson en lui donnant l'id de la person sur laquelle on a cliqué
6. Rajouter un app-person-modal avec un ngIf sur selected et lui assigner en person le selected
